﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Ticket;

namespace Theatre.BLL.Models.Spectacle
{
    public class SpectacleModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public string Author { get; set; } = null!;
        public DateTime Date { get; set; }
    }
}
