﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Theatre.DAL.Migrations
{
    public partial class changecolumnunique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TicketCategory",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_TicketState_Name",
                table: "TicketState",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TicketCategory_Name",
                table: "TicketCategory",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TicketState_Name",
                table: "TicketState");

            migrationBuilder.DropIndex(
                name: "IX_TicketCategory_Name",
                table: "TicketCategory");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TicketCategory",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");
        }
    }
}
