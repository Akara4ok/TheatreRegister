﻿using Theatre.DAL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DAL.Repositories.Interfaces
{
    public interface IPlaceRepository : IRepository<Place>
    {
    }
}
