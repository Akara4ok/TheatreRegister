﻿using Microsoft.EntityFrameworkCore;
using Theatre.DAL.Models;
using Theatre.DLL.Models;

namespace Theatre.DLL.DBContext
{
    public class TheatreDBContext : DbContext
    {
#nullable disable
        public DbSet<Spectacle> Spectacles { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<TicketCategory> TicketCategories { get; set; }
        public DbSet<TicketState> TicketStates { get; set; }
#nullable enable

        public TheatreDBContext(DbContextOptions<TheatreDBContext> options) : base(options) { }
    }
}
