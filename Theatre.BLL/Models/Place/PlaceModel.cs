﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Spectacle;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Models.TicketState;

namespace Theatre.BLL.Models.Place
{
    public class PlaceModel
    {
        public Guid Id { get; set; }
        public string Category { get; set; } = null!;
        public int Number { get; set; }
    }
}
