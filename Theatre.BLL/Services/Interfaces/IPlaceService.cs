﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Place;

namespace Theatre.BLL.Services.Interfaces
{
    public interface IPlaceService
    {
        Task<PlaceModel?> GetById(Guid id);
        Task<IEnumerable<PlaceModel>> GetAll();
        Task<PlaceModel?> Create(CreatePlaceModel newSpectacle);
        Task<PlaceModel?> Update(Guid id, UpdatePlaceModel updatedSpectacle);
        Task<PlaceModel?> Delete(Guid id);
    }
}
