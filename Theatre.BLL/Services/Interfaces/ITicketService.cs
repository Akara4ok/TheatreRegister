﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Ticket;

namespace Theatre.BLL.Services.Interfaces
{
    public interface ITicketService
    {
        Task<TicketModel?> GetById(Guid id);
        Task<IEnumerable<TicketModel>> GetAll(Guid? spectacleId);
        Task<TicketModel?> Create(CreateTicketModel newSpectacle);
        Task<TicketModel?> Update(Guid id, UpdateTicketModel updatedSpectacle);
        Task<TicketModel?> Delete(Guid id);
    }
}
