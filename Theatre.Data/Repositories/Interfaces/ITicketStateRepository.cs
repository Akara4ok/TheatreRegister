﻿using Theatre.DLL.Models;

namespace Theatre.DLL.Repositories.Interfaces
{
    public interface ITicketStateRepository : IRepository<TicketState>
    {
        Task<TicketState?> GetByName(string Name);
    }
}
