﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Spectacle;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Models.TicketState;

namespace Theatre.BLL.Models.Ticket
{
    public class CreateTicketModel
    {
        [Required]
        public Guid PlaceId { get; set; }
        [Required]
        public Guid SpectacleId { get; set; }
        [Required]
        public Guid StateId { get; set; }
    }
}
