﻿using Microsoft.AspNetCore.Mvc;
using Theatre.BLL.Models.Place;
using Theatre.BLL.Services.Interfaces;

namespace TheatreRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlaceController : ControllerBase
    {
        public readonly IPlaceService _placeService;

        public PlaceController(IPlaceService placeService)
        {
            _placeService = placeService;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPlace(Guid id)
        {
            var result = await _placeService.GetById(id);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetPlaces()
        {
            var result = await _placeService.GetAll();
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> CreatePlaces([FromBody] CreatePlaceModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _placeService.Create(model);

            return result != null ? StatusCode(StatusCodes.Status201Created, result) : BadRequest();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdatePlaces(Guid id, [FromBody] UpdatePlaceModel model)
        {
            var result = await _placeService.Update(id, model);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeletePlace(Guid id)
        {
            var result = await _placeService.Delete(id);

            return result != null ? Ok(result) : NotFound();
        }
    }
}
