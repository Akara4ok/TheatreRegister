﻿using System.Text.Json;

namespace TheatreRegister.Middleware
{
    public class ExceptionMiddleware : IMiddleware
    {
        public ExceptionMiddleware() { }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next.Invoke(context);
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(context, exception);
            }
        }

        private static async Task HandleErrorAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            var statusCode = exception switch
            {
                ArgumentException => 400,
                _ => 500
            };

            context.Response.StatusCode = statusCode;
            var errorMessage = JsonSerializer.Serialize(new
            {
                StatusCode = statusCode,
                ErrorMessage = statusCode != 500 ? exception.Message : "Internal error"
            });

            await context.Response.WriteAsync(errorMessage);
        }

    }
}
