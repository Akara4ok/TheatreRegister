﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Theatre.DLL.Models
{
    [Table("Spectacle")]
    public class Spectacle
    {
        [Key]
        public Guid Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; } = null!;
        [MaxLength(100)]
        public string Genre { get; set; } = null!;
        [MaxLength(100)]
        public string Author { get; set; } = null!;
        public DateTime Date { get; set; }
        public List<Ticket> Tickets { get; set; } = new();
    }
}
