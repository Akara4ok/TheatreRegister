﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Theatre.DLL.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll(Expression<Func<TEntity, bool>>? filter = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);

        Task<TEntity?> GetById(Guid id);

        Task<TEntity> Add(TEntity entity);

        TEntity Delete(TEntity entity);

        Task<TEntity?> DeleteById(Guid id);

        TEntity Update(TEntity entity);
    }
}
