﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Theatre.DLL.Models
{
    [Table("TicketCategory")]
    [Index(nameof(Name), IsUnique = true)]
    public class TicketCategory
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public float Price { get; set; }
    }
}
