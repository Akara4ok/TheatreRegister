﻿using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Linq;
using Theatre.DAL.Models;
using Theatre.DLL.DBContext;
using Theatre.DLL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL.Repositories
{
    public class TicketRepository : Repository<TheatreDBContext, Ticket>, ITicketRepository
    {
        public TicketRepository(TheatreDBContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Ticket>> GetAll(
            Expression<Func<Ticket, bool>>? filter = null,
            Func<IQueryable<Ticket>, IIncludableQueryable<Ticket, object>>? include = null)
        {
            IQueryable<Ticket> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.Include(t => t.Place)
                .ThenInclude(c => c.Category)
                .Include(t => t.Spectacle)
                .Include(t => t.State)
                .ToListAsync();
        }


        public override async Task<Ticket?> GetById(Guid id)
        {
            return await DbSet.Where(i => i.Id == id).Include(t => t.Place)
                .ThenInclude(c => c.Category)
                .Include(t => t.Spectacle)
                .Include(t => t.State)
                .FirstOrDefaultAsync();
        }
    }
}
