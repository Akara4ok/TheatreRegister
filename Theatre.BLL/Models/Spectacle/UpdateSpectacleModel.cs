﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.Spectacle
{
    public class UpdateSpectacleModel
    {
        [Required]
        public string Name { get; set; } = null!;
        [Required]
        public string Genre { get; set; } = null!;
        [Required]
        public string Author { get; set; } = null!;
        [Required]
        public DateTime? Date { get; set; }
    }
}
