using AutoMapper;
using Moq;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Profiles;
using Theatre.BLL.Services;
using Theatre.BLL.Services.Interfaces;
using Theatre.DLL;
using Theatre.DLL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.Tests
{
    public class CategoryServiceTests
    {
        private readonly ITicketCategoryService _service;
        private readonly Mock<IUnitOfWork> _unitOfWork = new();
        private readonly IMapper _mapper;

        public CategoryServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new TicketCategoryProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            _mapper = mapper;
            _service = new TicketCategoryService(_unitOfWork.Object, _mapper);
        }

        [Fact]
        public async Task GetById_ShouldReturnCategory_WhenCategoryExists() {
            //Arrange
            var categoryId = Guid.NewGuid();
            var categoryName = "B";
            var categoryPrice = 10.5f;
            var categoryModel = new TicketCategory { 
                Id = categoryId,
                Name = categoryName,
                Price = categoryPrice
            };

            _unitOfWork.Setup(x => x.TicketCategories.GetById(categoryId))
                .ReturnsAsync(categoryModel);

            //_mapper.Setup(m => m.Map<TicketCategoryModel>(categoryModel)).Returns(new TicketCategoryModel { Id = categoryModel.Id, Name = categoryModel.Name, Price = categoryModel.Price });


            //Act
            var category = await _service.GetById(categoryId);

            //Arrange
            Assert.Equal(categoryId, category?.Id);
            Assert.Equal(categoryName, category?.Name);
            Assert.Equal(categoryPrice, category?.Price);
        }

        [Fact]
        public async Task GetAll_ShouldReturnEmptyArray_WhenCategoryNotExist()
        {
            //Arrange
            var categories = Array.Empty<TicketCategory>();

            _unitOfWork.Setup(x => x.TicketCategories.GetAll(null, null))
                .ReturnsAsync(categories);

            //Act
            var category = await _service.GetAll();

            //Arrange
            Assert.Equal(categories.Length, category.Count());
        }

        [Fact]
        public async Task Create_ShouldReturnNewCreatedEntities()
        {
            //Arrange
            var categoryId = Guid.NewGuid();
            var categoryName = "E";
            var categoryPrice = 10.5f;
            var categoryViewModel = new CreateTicketCategoryModel
            {
                Name = categoryName,
                Price = categoryPrice
            };

            var category = new TicketCategory
            {
                Id = categoryId,
                Name = categoryName,
                Price = categoryPrice
            };

            _unitOfWork.Setup(x => x.TicketCategories.GetById(categoryId))
                .ReturnsAsync(category);
            _unitOfWork.Setup(x => x.TicketCategories.Add(category))
                .ReturnsAsync(category);

            //Act
            var result = await _service.Create(categoryViewModel);

            //Arrange
            Assert.Equal(categoryName, result?.Name);
            Assert.Equal(categoryPrice, result?.Price);
        }

        [Fact]
        public async Task Update_ShouldReturnNewUpdatedEntities_WhenCategoryExist()
        {
            //Arrange
            var categoryId = Guid.NewGuid();
            var categoryName = "E";
            var categoryPrice = 10.5f;
            var categoryViewModel = new UpdateTicketCategoryModel
            {
                Name = categoryName,
                Price = categoryPrice
            };

            var category = new TicketCategory
            {
                Id = categoryId,
                Name = categoryName,
                Price = categoryPrice
            };

            _unitOfWork.Setup(x => x.TicketCategories.GetById(categoryId))
                .ReturnsAsync(category);
            _unitOfWork.Setup(x => x.TicketCategories.Update(category))
                .Returns(category);

            //Act
            var result = await _service.Update(categoryId, categoryViewModel);

            //Arrange
            Assert.Equal(categoryId, result?.Id);
            Assert.Equal(categoryName, result?.Name);
            Assert.Equal(categoryPrice, result?.Price);
        }

        [Fact]
        public async Task Delete_ShouldReturnDeletedEntities_WhenCategoryExist()
        {
            //Arrange
            var categoryId = Guid.NewGuid();
            var categoryName = "E";
            var categoryPrice = 10.5f;

            var category = new TicketCategory
            {
                Id = categoryId,
                Name = categoryName,
                Price = categoryPrice
            };

            _unitOfWork.Setup(x => x.TicketCategories.GetById(categoryId))
                .ReturnsAsync(category);
            _unitOfWork.Setup(x => x.TicketCategories.DeleteById(categoryId))
                .ReturnsAsync(category);
            //_mapper.Setup(m => m.Map<TicketCategoryModel>(category)).Returns(new TicketCategoryModel { Id = category.Id, Name = category.Name, Price = category.Price });

            //Act
            var result = await _service.Delete(categoryId);

            //Arrange
            Assert.Equal(categoryId, result?.Id);
            Assert.Equal(categoryName, result?.Name);
            Assert.Equal(categoryPrice, result?.Price);
        }
    }
}