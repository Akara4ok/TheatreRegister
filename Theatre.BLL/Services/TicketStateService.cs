﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketState;
using Theatre.BLL.Services.Interfaces;
using Theatre.DLL;
using Theatre.DLL.Models;

namespace Theatre.BLL.Services
{
    public class TicketStateService : ITicketStateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TicketStateService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<TicketStateModel?> GetById(Guid id)
        {
            var ticketState = await _unitOfWork.TicketStates.GetById(id);
            return ticketState is null ? null : _mapper.Map<TicketStateModel>(ticketState);
        }

        public async Task<IEnumerable<TicketStateModel>> GetAll()
        {
            var ticketStates = await _unitOfWork.TicketStates.GetAll();

            return _mapper.Map<IEnumerable<TicketStateModel>>(ticketStates);
        }

        public async Task<TicketStateModel> Create(CreateTicketStateModel newTicketState)
        {
            var state = new TicketState();
            _mapper.Map(newTicketState, state);
            await _unitOfWork.TicketStates.Add(state);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketStateModel>(state);
        }

        public async Task<TicketStateModel?> Update(Guid id, UpdateTicketStateModel updatedTicketState)
        {
            var state = await _unitOfWork.TicketStates.GetById(id);
            if (state is null)
            {
                return null;
            }
            _mapper.Map(updatedTicketState, state);
            _unitOfWork.TicketStates.Update(state);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketStateModel>(state);
        }

        public async Task<TicketStateModel?> Delete(Guid id)
        {
            var state = await _unitOfWork.TicketStates.DeleteById(id);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketStateModel>(state);
        }
    }
}
