﻿using Theatre.DAL.Repositories.Interfaces;
using Theatre.DLL.DBContext;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL
{
    public class UnitOfWork : IUnitOfWork
    {
        public ISpectacleRepository Spectacles { get; }
        public ITicketRepository Tickets { get; }
        public IPlaceRepository Places { get; }
        public ITicketStateRepository TicketStates { get; }
        public ITicketCategoryRepository TicketCategories { get; }

        private readonly TheatreDBContext _context;

        public UnitOfWork(TheatreDBContext context,
                            ISpectacleRepository spectacles,
                            ITicketRepository tickets,
                            IPlaceRepository places,
                            ITicketStateRepository ticketStates,
                            ITicketCategoryRepository ticketCategories)
        {
            _context = context;
            Spectacles = spectacles;
            Tickets = tickets;
            TicketStates = ticketStates;
            Places = places;
            TicketCategories = ticketCategories;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
