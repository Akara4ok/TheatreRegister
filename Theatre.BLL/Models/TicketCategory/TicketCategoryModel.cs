﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.TicketCategory
{
    public class TicketCategoryModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public float Price { get; set; }
    }
}
