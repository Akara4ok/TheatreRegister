﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Ticket;
using Theatre.DLL.Models;

namespace Theatre.BLL.Profiles
{
    public class TicketProfile : Profile
    {
        public TicketProfile() 
        {
            CreateMap<Ticket, TicketModel>()
                .ForMember(x => x.Place, options => options.MapFrom(x => x.Place.Number))
                .ForMember(x => x.Category, options => options.MapFrom(x => x.Place.Category.Name))
                .ForMember(x => x.Price, options => options.MapFrom(x => x.Place.Category.Price))
                .ForMember(x => x.Spectacle, options => options.MapFrom(x => x.Spectacle.Name))
                .ForMember(x => x.Date, options => options.MapFrom(x => x.Spectacle.Date))
                .ForMember(x => x.State, options => options.MapFrom(x => x.State.Name));
            CreateMap<CreateTicketModel, Ticket>();
            CreateMap<UpdateTicketModel, Ticket>();
            CreateMap<TicketModel, Ticket>();
        }
    }
}
