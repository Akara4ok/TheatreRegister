﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Services.Interfaces;
using Theatre.DLL;
using Theatre.DLL.Models;

namespace Theatre.BLL.Services
{
    public class TicketCategoryService : ITicketCategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TicketCategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<TicketCategoryModel?> GetById(Guid id)
        {
            var ticketCategory = await _unitOfWork.TicketCategories.GetById(id);
            return ticketCategory is null ? null : _mapper.Map<TicketCategoryModel>(ticketCategory);
        }

        public async Task<IEnumerable<TicketCategoryModel>> GetAll()
        {
            var ticketCategories = await _unitOfWork.TicketCategories.GetAll();

            return _mapper.Map<IEnumerable<TicketCategoryModel>>(ticketCategories);
        }

        public async Task<TicketCategoryModel> Create(CreateTicketCategoryModel newTicketCategory)
        {
            var category = new TicketCategory();
            _mapper.Map(newTicketCategory, category);
            await _unitOfWork.TicketCategories.Add(category);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketCategoryModel>(category);
        }

        public async Task<TicketCategoryModel?> Update(Guid id, UpdateTicketCategoryModel updatedTicketCategory)
        {
            var category = await _unitOfWork.TicketCategories.GetById(id);
            if (category is null)
            {
                return null;
            }
            _mapper.Map(updatedTicketCategory, category);
            _unitOfWork.TicketCategories.Update(category);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketCategoryModel>(category);
        }

        public async Task<TicketCategoryModel?> Delete(Guid id)
        {
            var category = await _unitOfWork.TicketCategories.DeleteById(id);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketCategoryModel>(category);
        }
    }
}
