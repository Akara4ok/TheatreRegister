﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Place;
using Theatre.BLL.Models.Spectacle;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Models.TicketState;

namespace Theatre.BLL.Models.Ticket
{
    public class TicketModel
    {
        public Guid Id { get; set; }
        public int? Place { get; set; }
        public string Category { get; set; } = null!;
        public float? Price { get; set; }
        public string Spectacle { get; set; } = null!;
        public DateTime? Date { get; set; }
        public string State { get; set; } = null!;
    }
}
