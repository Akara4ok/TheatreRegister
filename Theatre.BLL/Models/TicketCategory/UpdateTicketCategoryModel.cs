﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.TicketCategory
{
    public class UpdateTicketCategoryModel
    {
        [Required]
        public string Name { get; set; } = null!;
        [Range(0, float.MaxValue, ErrorMessage = "Please enter a value bigger than {0}")]
        public float Price { get; set; }
    }
}
