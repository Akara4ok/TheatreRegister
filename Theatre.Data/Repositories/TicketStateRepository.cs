﻿using Microsoft.EntityFrameworkCore;
using Theatre.DLL.DBContext;
using Theatre.DLL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL.Repositories
{
    public class TicketStateRepository : Repository<TheatreDBContext, TicketState>, ITicketStateRepository
    {
        public TicketStateRepository(TheatreDBContext context) : base(context)
        {
        }

        public async Task<TicketState?> GetByName(string Name)
        {
            return await DbSet.FirstAsync(p => p.Name == Name);
        }
    }
}
