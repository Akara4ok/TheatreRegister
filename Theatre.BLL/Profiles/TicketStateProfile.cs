﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketState;
using Theatre.DLL.Models;

namespace Theatre.BLL.Profiles
{
    public class TicketStateProfile : Profile
    {
        public TicketStateProfile() 
        {
            CreateMap<TicketState, TicketStateModel>();
            CreateMap<CreateTicketStateModel, TicketState>();
            CreateMap<UpdateTicketStateModel, TicketState>();
            CreateMap<TicketStateModel, TicketState>();
        }
    }
}
