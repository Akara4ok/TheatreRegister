﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketState;

namespace Theatre.BLL.Services.Interfaces
{
    public interface ITicketStateService
    {
        Task<TicketStateModel?> GetById(Guid id);
        Task<IEnumerable<TicketStateModel>> GetAll();
        Task<TicketStateModel> Create(CreateTicketStateModel newTicketState);
        Task<TicketStateModel?> Update(Guid id, UpdateTicketStateModel updatedTicketState);
        Task<TicketStateModel?> Delete(Guid id);
    }
}
