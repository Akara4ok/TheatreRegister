﻿using Theatre.DLL.DBContext;
using Theatre.DLL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL.Repositories
{
    public class TicketCategoryRepository : Repository<TheatreDBContext, TicketCategory>, ITicketCategoryRepository
    {
        public TicketCategoryRepository(TheatreDBContext context) : base(context)
        {
        }
    }
}
