﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Spectacle;
using Theatre.BLL.Models.Ticket;
using Theatre.BLL.Services.Interfaces;
using Theatre.DLL;
using Theatre.DLL.Models;

namespace Theatre.BLL.Services
{
    public class SpectacleService : ISpectacleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITicketService _tickets;

        public SpectacleService(IUnitOfWork unitOfWork, IMapper mapper, ITicketService tickets)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _tickets = tickets;
        }

        public async Task<SpectacleModel?> GetById(Guid id)
        {
            var spectacle = await _unitOfWork.Spectacles.GetById(id);
            return spectacle is null ? null : _mapper.Map<SpectacleModel>(spectacle);
        }

        public async Task<IEnumerable<SpectacleModel>> GetAll()
        {
            var spectacles = await _unitOfWork.Spectacles.GetAll();

            return _mapper.Map<IEnumerable<SpectacleModel>>(spectacles);
        }

        public async Task<SpectacleModel> Create(CreateSpectacleModel newSpectacle)
        {
            var spectacle = new Spectacle();
            _mapper.Map(newSpectacle, spectacle);
            await _unitOfWork.Spectacles.Add(spectacle);

            var places = await _unitOfWork.Places.GetAll();
            var state = await _unitOfWork.TicketStates.GetByName("Available");
            if (state is null)
            {
                throw new RowNotInTableException("The state available is not exist");
            }
            foreach (var place in places) {
                CreateTicketModel ticket = new();
                ticket.SpectacleId = spectacle.Id;
                ticket.PlaceId = place.Id;
                ticket.StateId = state.Id;
                await _tickets.Create(ticket);
            }

            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<SpectacleModel>(spectacle);
        }

        public async Task<SpectacleModel?> Update(Guid id, UpdateSpectacleModel updateSpectacle)
        {
            var spectacle = await _unitOfWork.Spectacles.GetById(id);
            if (spectacle is null)
            {
                return null;
            }
            _mapper.Map(updateSpectacle, spectacle);
            _unitOfWork.Spectacles.Update(spectacle);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<SpectacleModel>(spectacle);
        }

        public async Task<SpectacleModel?> Delete(Guid id)
        {
            var spectacle = await _unitOfWork.Spectacles.DeleteById(id);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<SpectacleModel>(spectacle);
        }
    }
}
