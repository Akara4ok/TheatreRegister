﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Theatre.DLL.Models
{
    [Table("TicketState")]
    [Index(nameof(Name), IsUnique = true)]
    public class TicketState
    {
        [Key]
        public Guid Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; } = null!;
    }
}
