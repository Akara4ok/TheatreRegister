﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.TicketState
{
    public class UpdateTicketStateModel
    {
        [Required]
        public string Name { get; set; } = null!;
    }
}
