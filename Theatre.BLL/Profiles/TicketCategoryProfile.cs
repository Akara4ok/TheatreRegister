﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketCategory;
using Theatre.DLL.Models;

namespace Theatre.BLL.Profiles
{
    public class TicketCategoryProfile : Profile
    {
        public TicketCategoryProfile() 
        {
            CreateMap<TicketCategory, TicketCategoryModel>();
            CreateMap<CreateTicketCategoryModel, TicketCategory>();
            CreateMap<UpdateTicketCategoryModel, TicketCategory>();
            CreateMap<TicketCategoryModel, TicketCategory>();
        }
    }
}
