﻿using Microsoft.AspNetCore.Mvc;
using Theatre.BLL.Models.Spectacle;
using Theatre.BLL.Services.Interfaces;

namespace TheatreRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SpectacleController : ControllerBase
    {
        public readonly ISpectacleService _spectacleService;

        public SpectacleController(ISpectacleService spectacleService)
        {
            _spectacleService = spectacleService;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetSpectacle(Guid id)
        {
            var result = await _spectacleService.GetById(id);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetSpectacles()
        {
            var result = await _spectacleService.GetAll();
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> CreateSpectacles([FromBody] CreateSpectacleModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _spectacleService.Create(model);

            return result != null ? StatusCode(StatusCodes.Status201Created, result) : BadRequest();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateSpectacles(Guid id, [FromBody] UpdateSpectacleModel model)
        {
            var result = await _spectacleService.Update(id, model);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteSpectacle(Guid id)
        {
            var result = await _spectacleService.Delete(id);

            return result != null ? Ok(result) : NotFound();
        }
    }
}
