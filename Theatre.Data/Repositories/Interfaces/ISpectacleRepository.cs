﻿using Theatre.DLL.Models;

namespace Theatre.DLL.Repositories.Interfaces
{
    public interface ISpectacleRepository : IRepository<Spectacle>
    {
    }
}
