﻿using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Linq;
using Theatre.DAL.Models;
using Theatre.DLL.DBContext;
using Theatre.DLL.Models;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL.Repositories
{
    public class SpectacleRepository : Repository<TheatreDBContext, Spectacle>, ISpectacleRepository
    {
        public SpectacleRepository(TheatreDBContext context) : base(context)
        {
        }
    }
}
