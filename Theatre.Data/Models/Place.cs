﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Theatre.DLL.Models;

namespace Theatre.DAL.Models
{
    [Index(nameof(Number), IsUnique = true)]
    public class Place
    {
        [Key]
        public Guid Id { get; set; }
        public TicketCategory Category { get; set; } = null!;
        public int Number { get; set; }
    }
}
