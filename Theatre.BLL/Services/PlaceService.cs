﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Place;
using Theatre.BLL.Services.Interfaces;
using Theatre.DAL.Models;
using Theatre.DLL;

namespace Theatre.BLL.Services
{
    public class PlaceService : IPlaceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PlaceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PlaceModel?> GetById(Guid id)
        {
            var place = await _unitOfWork.Places.GetById(id);
            return place is null ? null : _mapper.Map<PlaceModel>(place);
        }

        public async Task<IEnumerable<PlaceModel>> GetAll()
        {
            var places = await _unitOfWork.Places.GetAll();

            return _mapper.Map<IEnumerable<PlaceModel>>(places);
        }

        public async Task<PlaceModel?> Create(CreatePlaceModel newPlace)
        {
            var category = await _unitOfWork.TicketCategories.GetById(newPlace.CategoryId);
            if(category is null) {
                throw new ArgumentException("This category is not exist");
            }
            var place = new Place();
            _mapper.Map(newPlace, place);
            place.Category = category;
            await _unitOfWork.Places.Add(place);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<PlaceModel>(place);
        }

        public async Task<PlaceModel?> Update(Guid id, UpdatePlaceModel updatedPlace)
        {
            var category = await _unitOfWork.TicketCategories.GetById(updatedPlace.CategoryId);
            if (category is null)
            {
                throw new ArgumentException("This category is not exist");
            }

            var place = await _unitOfWork.Places.GetById(id);
            if (place is null)
            {
                return null;
            }
            _mapper.Map(updatedPlace, place);
            place.Category = category;
            _unitOfWork.Places.Update(place);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<PlaceModel>(place);
        }

        public async Task<PlaceModel?> Delete(Guid id)
        {
            var place = await _unitOfWork.Places.DeleteById(id);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<PlaceModel>(place);
        }
    }
}
