﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Theatre.DAL.Models;

namespace Theatre.DLL.Models
{
    [Table("Ticket")]
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }
        public Place Place { get; set; } = null!;
        public Spectacle Spectacle { get; set; } = null!;
        public TicketState State { get; set; } = null!;
    }
}
