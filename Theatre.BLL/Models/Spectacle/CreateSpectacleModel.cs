﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Ticket;

namespace Theatre.BLL.Models.Spectacle
{
    public class CreateSpectacleModel
    {
        [Required]
        public string Name { get; set; } = null!;
        [Required]
        public string Genre { get; set; } = null!;
        [Required]
        public string Author { get; set; } = null!;
        [Required]
        public DateTime? Date { get; set; }
    }
}
