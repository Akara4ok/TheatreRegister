﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.TicketState
{
    public class TicketStateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
