﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.TicketCategory;

namespace Theatre.BLL.Services.Interfaces
{
    public interface ITicketCategoryService
    {
        Task<TicketCategoryModel?> GetById(Guid id);
        Task<IEnumerable<TicketCategoryModel>> GetAll();
        Task<TicketCategoryModel> Create(CreateTicketCategoryModel newTicketCategory);
        Task<TicketCategoryModel?> Update(Guid id, UpdateTicketCategoryModel updatedTicketCategory);
        Task<TicketCategoryModel?> Delete(Guid id);
    }
}
