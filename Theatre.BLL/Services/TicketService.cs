﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Ticket;
using Theatre.BLL.Services.Interfaces;
using Theatre.DAL.Models;
using Theatre.DLL;
using Theatre.DLL.Models;

namespace Theatre.BLL.Services
{
    public class TicketService : ITicketService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TicketService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<TicketModel?> GetById(Guid id)
        {
            var ticket = await _unitOfWork.Tickets.GetById(id);
            return ticket is null ? null : _mapper.Map<TicketModel>(ticket);
        }

        public async Task<IEnumerable<TicketModel>> GetAll(Guid? spectacleId)
        {
            if (spectacleId != null) { 
                var spectacle_tickets = await _unitOfWork.Tickets.GetAll(x => x.Spectacle.Id == spectacleId);
                return _mapper.Map<IEnumerable<TicketModel>>(spectacle_tickets);
            }
            var tickets = await _unitOfWork.Tickets.GetAll();
            return _mapper.Map<IEnumerable<TicketModel>>(tickets);
        }

        public async Task<TicketModel?> Create(CreateTicketModel newTicket)
        {
            var place = await _unitOfWork.Places.GetById(newTicket.PlaceId);
            var spectacle = await _unitOfWork.Spectacles.GetById(newTicket.SpectacleId);
            var ticketState = await _unitOfWork.TicketStates.GetById(newTicket.StateId);
            if (place is null)
            {
                throw new ArgumentException("This place is not exist");
            }
            if (spectacle is null)
            {
                throw new ArgumentException("This spectacle is not exist");
            }
            if (ticketState is null)
            {
                throw new ArgumentException("This ticketState is not exist");
            }

            var ticket = new Ticket();
            _mapper.Map(newTicket, ticket);
            ticket.Place = place;
            ticket.Spectacle = spectacle;
            ticket.State = ticketState;

            await _unitOfWork.Tickets.Add(ticket);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketModel>(ticket);
        }

        public async Task<TicketModel?> Update(Guid id, UpdateTicketModel updatedTicket)
        {
            var place = await _unitOfWork.Places.GetById(updatedTicket.PlaceId);
            var spectacle = await _unitOfWork.Spectacles.GetById(updatedTicket.SpectacleId);
            var ticketState = await _unitOfWork.TicketStates.GetById(updatedTicket.StateId);
            if (place is null || spectacle is null || ticketState is null)
            {
                return null;
            }
            var ticket = await _unitOfWork.Tickets.GetById(id);
            if (ticket is null)
            {
                return null;
            }
            _mapper.Map(updatedTicket, ticket);
            ticket.Place = place;
            ticket.Spectacle = spectacle;
            ticket.State = ticketState;
            _unitOfWork.Tickets.Update(ticket);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketModel>(ticket);
        }

        public async Task<TicketModel?> Delete(Guid id)
        {
            var ticket = await _unitOfWork.Tickets.DeleteById(id);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TicketModel>(ticket);
        }
    }
}
