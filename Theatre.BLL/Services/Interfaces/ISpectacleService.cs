﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Spectacle;

namespace Theatre.BLL.Services.Interfaces
{
    public interface ISpectacleService
    {
        Task<SpectacleModel?> GetById(Guid id);
        Task<IEnumerable<SpectacleModel>> GetAll();
        Task<SpectacleModel> Create(CreateSpectacleModel newSpectacle);
        Task<SpectacleModel?> Update(Guid id, UpdateSpectacleModel updatedSpectacle);
        Task<SpectacleModel?> Delete(Guid id);
    }
}
