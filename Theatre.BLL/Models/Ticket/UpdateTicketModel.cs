﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre.BLL.Models.Ticket
{
    public class UpdateTicketModel
    {
        [Required]
        public Guid PlaceId { get; set; }
        [Required]
        public Guid SpectacleId { get; set; }
        [Required]
        public Guid StateId { get; set; }
    }
}
