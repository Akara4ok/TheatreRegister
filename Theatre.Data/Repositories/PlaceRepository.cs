﻿using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Linq;
using Theatre.DAL.Models;
using Theatre.DAL.Repositories.Interfaces;
using Theatre.DLL.DBContext;
using Theatre.DLL.Repositories;

namespace Theatre.DAL.Repositories
{
    public class PlaceRepository : Repository<TheatreDBContext, Place>, IPlaceRepository
    {
        public PlaceRepository(TheatreDBContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Place>> GetAll(
            Expression<Func<Place, bool>>? filter = null,
            Func<IQueryable<Place>, IIncludableQueryable<Place, object>>? include = null)
        {
            IQueryable<Place> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.Include(p => p.Category).ToListAsync();
        }


        public override async Task<Place?> GetById(Guid id)
        {
            return await DbSet.Include(p => p.Category).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
