﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Spectacle;
using Theatre.DLL.Models;

namespace Theatre.BLL.Profiles
{
    public class SpectacleProfile : Profile
    {
        public SpectacleProfile() { 
            CreateMap<Spectacle, SpectacleModel>();
            CreateMap<CreateSpectacleModel, Spectacle>();
            CreateMap<UpdateSpectacleModel, Spectacle>();
            CreateMap<SpectacleModel, Spectacle>();
        }
    }
}
