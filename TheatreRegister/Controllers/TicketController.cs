﻿using Microsoft.AspNetCore.Mvc;
using Theatre.BLL.Models.Ticket;
using Theatre.BLL.Services.Interfaces;

namespace TheatreRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketController : ControllerBase
    {
        public readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetTicket(Guid id)
        {
            var result = await _ticketService.GetById(id);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetTickets([FromQuery] Guid? spectacleId)
        {
            var result = await _ticketService.GetAll(spectacleId);
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> CreateTicket([FromBody] CreateTicketModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _ticketService.Create(model);

            return result != null ? StatusCode(StatusCodes.Status201Created, result) : BadRequest();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateTicket(Guid id, [FromBody] UpdateTicketModel model)
        {
            var result = await _ticketService.Update(id, model);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTicket(Guid id)
        {
            var result = await _ticketService.Delete(id);

            return result != null ? Ok(result) : NotFound();
        }
    }
}
