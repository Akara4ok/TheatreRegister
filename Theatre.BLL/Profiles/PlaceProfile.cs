﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Theatre.BLL.Models.Place;
using Theatre.DAL.Models;

namespace Theatre.BLL.Profiles
{
    public class PlaceProfile : Profile
    {
        public PlaceProfile() {
            CreateMap<Place, PlaceModel>().ForMember(x => x.Category, options => options.MapFrom(x => x.Category.Name));
            CreateMap<CreatePlaceModel, Place>();
            CreateMap<UpdatePlaceModel, Place>();
            CreateMap<PlaceModel, Place>();
        }
    }
}
