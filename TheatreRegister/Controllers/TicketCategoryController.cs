﻿using Microsoft.AspNetCore.Mvc;
using Theatre.BLL.Models.TicketCategory;
using Theatre.BLL.Services.Interfaces;

namespace TheatreRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketCategoryController : ControllerBase
    {
        public readonly ITicketCategoryService _ticketCategoryService;

        public TicketCategoryController(ITicketCategoryService ticketCategoryService)
        {
            _ticketCategoryService = ticketCategoryService;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetTicketCategory(Guid id)
        {
            var result = await _ticketCategoryService.GetById(id);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetTicketCategories()
        {
            var result = await _ticketCategoryService.GetAll();
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> CreateTicketCategories([FromBody] CreateTicketCategoryModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _ticketCategoryService.Create(model);

            return result != null ? StatusCode(StatusCodes.Status201Created, result) : BadRequest();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateTicketCategories(Guid id, [FromBody] UpdateTicketCategoryModel model)
        {
            var result = await _ticketCategoryService.Update(id, model);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTicketCategory(Guid id)
        {
            var result = await _ticketCategoryService.Delete(id);

            return result != null ? Ok(result) : NotFound();
        }
    }
}
