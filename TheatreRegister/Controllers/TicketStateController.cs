﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Theatre.BLL.Models.TicketState;
using Theatre.BLL.Services.Interfaces;

namespace TheatreRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketStateController : ControllerBase
    {
        public readonly ITicketStateService _ticketStateService;

        public TicketStateController(ITicketStateService ticketStateService)
        {
            _ticketStateService = ticketStateService;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetTicketState(Guid id) 
        {
            var result = await _ticketStateService.GetById(id);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetTicketStates() 
        {
            var result = await _ticketStateService.GetAll();
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> CreateTicketStates([FromBody] CreateTicketStateModel model)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _ticketStateService.Create(model);

            return result != null ? StatusCode(StatusCodes.Status201Created, result) : BadRequest();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateTicketStates(Guid id, [FromBody] UpdateTicketStateModel model)
        {
            var result = await _ticketStateService.Update(id, model);

            return result != null ? Ok(result) : NotFound();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTicketState(Guid id)
        {
            var result = await _ticketStateService.Delete(id);

            return result != null ? Ok(result) : NotFound();
        }
    }
}