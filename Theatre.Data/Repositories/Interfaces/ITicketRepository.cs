﻿using Theatre.DLL.Models;

namespace Theatre.DLL.Repositories.Interfaces
{
    public interface ITicketRepository : IRepository<Ticket>
    {
    }
}
