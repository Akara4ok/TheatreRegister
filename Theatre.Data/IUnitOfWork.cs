﻿using Theatre.DAL.Repositories.Interfaces;
using Theatre.DLL.Repositories.Interfaces;

namespace Theatre.DLL
{
    public interface IUnitOfWork
    {
        ISpectacleRepository Spectacles { get; }
        ITicketRepository Tickets { get; }
        IPlaceRepository Places { get; }
        ITicketStateRepository TicketStates { get; }
        ITicketCategoryRepository TicketCategories { get; }
        Task<int> SaveChangesAsync();
    }
}
